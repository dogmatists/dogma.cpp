// This is free and unencumbered software released into the public domain.

#pragma once

namespace dogma {
  struct Longitude;
}

// See: https://dogma.dev/Longitude/
struct dogma::Longitude {
  double radians;
};

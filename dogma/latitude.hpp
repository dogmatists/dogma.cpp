// This is free and unencumbered software released into the public domain.

#pragma once

namespace dogma {
  struct Latitude;
}

// See: https://dogma.dev/Latitude/
struct dogma::Latitude {
  double radians;
};

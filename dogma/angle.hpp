// This is free and unencumbered software released into the public domain.

#pragma once

namespace dogma {
  struct Angle;
}

// See: https://dogma.dev/Angle/
struct dogma::Angle {
  double radians;
};
